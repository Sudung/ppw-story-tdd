from django.apps import apps
from django.test import TestCase, Client
from .views import *

class LabTDDUnitTest(TestCase):
    def test_default_url_redirect(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)