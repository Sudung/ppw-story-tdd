$(document).ready(function() {
    $('#search').keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            searchAction(this);
            return false;
        }
    });
});

function favoriteAction(element) {
    let count = $("#counter").text();
    let state = $(element).attr('value');
    let token = $('input[name="csrfmiddlewaretoken"]').attr('value');

    if (state == 'False') {
        $(element).attr('class', 'btn btn-success btn-sm');
    } else {
        $(element).attr('class', 'btn btn-light btn-sm');
    }

    $.ajax({
        method: 'POST',
        url:('http://ppw-lab-tdd.herokuapp.com/lab_9/'),
        data : JSON.stringify ({'count' : count, 'state' : state}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function (response) {
            count = response.count;
            state = response.state;
            $("#counter").text(count);
            $(element).attr('value', state);
        },
    })
}

function buildTable(json_data) {
    content = $('tbody');
    content.empty();

    for (let i = 0; i < Object.keys(json_data).length; i++) {
        let title;
        let author;
        let publisher;
        let publishedDate;
        let endString;
        let finalString;

        title = '<td>' + json_data[i].volumeInfo.title + '</td>';

        author = '<td>' + json_data[i].volumeInfo.authors + '</td>';

        publisher = '<td>' + json_data[i].volumeInfo.publisher + '</td>';
        publishedDate = '<td>' + json_data[i].volumeInfo.publishedDate + '</td>';

        endString = '<form id = "favorite" method="POST">{% csrf_token %}<td><button value = "False" type = "button" onclick = "favoriteAction(this)" class = "btn btn-light btn-sm">Favorite</button></td></form>'

        finalString = '<tr>' + title + author + publisher + publishedDate + endString + '</tr>';
        $('tbody').append(finalString);

    }
}

function searchAction(element) {
    let key = $(element).val();
    let token = $('input[name="csrfmiddlewaretoken"]').attr('value');

    $.ajax({
        method: 'POST',
        url:('http://ppw-lab-tdd.herokuapp.com/lab_9/search/'),
        data : JSON.stringify ({'key' : key}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function (response) {
            data = response.data;
            buildTable(data);
            $("#counter").text('0');
        },
    })
}