from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from .apps import Lab8Config
from .views import *

class Lab8UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/lab_8/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab_8/')
        self.assertEqual(found.func, index)
        
    def test_using_index_template(self):
        response = Client().get('/lab_8/')
        self.assertTemplateUsed(response, 'lab_8/index.html')

    def test_apps(self):
        self.assertEqual(Lab8Config.name, 'lab_8')
        self.assertEqual(apps.get_app_config('lab_8').name, 'lab_8')