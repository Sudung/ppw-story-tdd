from django.urls import path
from .views import *

app_name = 'lab_8'

urlpatterns = [
    path('', index, name='index'),
]