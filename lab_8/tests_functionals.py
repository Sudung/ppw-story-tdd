from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class NewVisitorLandingTest(unittest.TestCase):  
    def setUp(self):  
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):  
        self.browser.quit()

    def test_retrieve_landing(self):
        self.browser.get('http://localhost:8000/lab_8/')
        time.sleep(3)
        self.assertIn('Story 8', self.browser.title)

        banner = self.browser.find_element_by_tag_name('h1').text
        self.assertEqual('Hello, Apa Kabar?', banner)

    def test_theme_changer(self):
        self.browser.get('http://localhost:8000/lab_8/')
        time.sleep(3)

        theme_changer = self.browser.find_element_by_id('theme-changer')
        self.assertEqual('rgba(0, 0, 0, 1)', self.browser.find_element_by_tag_name('body').value_of_css_property('color'))
        self.assertIn('btn-dark', theme_changer.get_attribute('class'))

        theme_changer.click()
        time.sleep(1)
        self.assertEqual('rgba(255, 255, 255, 1)', self.browser.find_element_by_tag_name('body').value_of_css_property('color'))
        self.assertIn('btn-light', theme_changer.get_attribute('class'))

        theme_changer.click()
        time.sleep(1)
        self.assertEqual('rgba(0, 0, 0, 1)', self.browser.find_element_by_tag_name('body').value_of_css_property('color'))
        self.assertIn('btn-dark', theme_changer.get_attribute('class'))

    def test_each_accordion_content_is_displayed(self):
        self.browser.get('http://localhost:8000/lab_8/')
        time.sleep(3)

        accordion_elements = self.browser.find_elements_by_class_name('accordion')

        current_content_id = 0
        for elem in accordion_elements:
            current_headers = elem.find_elements_by_class_name('ui-accordion-header')
            
            for header in current_headers:
                # Check if each accordion header's content is displayed by clicking on each header element
                header.click()
                time.sleep(1)
                current_content_id += 2

                # Each accordion header have a content, marked by id = 'ui-id-{even counter}'
                self.assertTrue(self.browser.find_element_by_id("ui-id-{}".format(current_content_id)).is_displayed())
                