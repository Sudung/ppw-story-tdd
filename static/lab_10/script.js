function checkValid() {
    let email = $('#form-sub .email-content').val();
    let name = $('#form-sub .name-content').val();
    let password = $('#form-sub .password-content').val();
    let token = $('input[name="csrfmiddlewaretoken"]').attr('value');

    $.ajax({
        method: 'POST',
        url:('http://ppw-lab-tdd.herokuapp.com/lab_10/validate/'),
        data : JSON.stringify({'email' : email, 'name' : name, 'password' : password,}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function(response) {
            let emailExist = response.email_is_exist;
            let formValid = response.form_is_valid;

            if (emailExist || !formValid) {
                $('#submit-button').prop("disabled", true);
                $('#form-info').prop("hidden", false);

                if (!formValid) {
                    $('#form-info').text("Form not valid!");
                } else if (emailExist) {
                    $('#form-info').text("Email already exists!");
                }
            } else {
                $('#submit-button').prop("disabled", false);
                $('#form-info').prop("hidden", false);
                $('#form-info').text("Form checks out!");
            }
        },
    })
}

function postForm() {
    event.preventDefault(true);
    let email = $('#form-sub .email-content').val();
    let name = $('#form-sub .name-content').val();
    let password = $('#form-sub .password-content').val();
    let token = $('#form-sub input[name="csrfmiddlewaretoken"]').attr('value');

    $.ajax({
        method: 'POST',
        url:('http://ppw-lab-tdd.herokuapp.com/lab_10/regist/'),
        data : JSON.stringify({'email' : email, 'name' : name, 'password' : password,}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function(response) {
            let formIsPosted = response.form_is_posted;

            if (formIsPosted) {
                $('#submit-button').prop("disabled", true);
                $('#form-info').prop("hidden", false);
                $('#form-info').text("User subscribed!");
            }
        }
    })
}

function unsubscribe() {
    event.preventDefault(true)
    let email_conf = $('#form-unsub .email-content').val();
    let password_conf = $('#form-unsub .password-content').val();
    let token = $('input[name="csrfmiddlewaretoken"]').attr('value');

    $.ajax({
        method: 'POST',
        url:('http://ppw-lab-tdd.herokuapp.com/lab_10/unsub/'),
        data : JSON.stringify({'email' : email_conf, 'password' : password_conf,}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function(response) {
            let passMatch = response.pass_match;
            let emailFound = response.email_found;

            if (passMatch && emailFound) {
                $('#form-info-2').prop("hidden", false);
                $('#form-info-2').text("User unsubscribed!");
            } else {
                $('#form-info-2').prop("hidden", false);
                if (!emailFound) {
                    $('#form-info-2').text("User with the specified email not found!");
                } else if (!passMatch) {
                    $('#form-info-2').text("Wrong password!");
                }
            }
        }
    })

}