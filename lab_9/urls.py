from django.urls import path
from .views import *

app_name = 'lab_9'

urlpatterns = [
    path('', index, name = 'index'),
    path('search/', search, name = 'search'),
]