from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from .apps import Lab9Config
from .views import *
import json

class Lab9UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/lab_9/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab_9/')
        self.assertEqual(found.func, index)
        
    def test_using_index_template(self):
        response = Client().get('/lab_9/')
        self.assertTemplateUsed(response, 'lab_9/index.html')

    def test_apps(self):
        self.assertEqual(Lab9Config.name, 'lab_9')
        self.assertEqual(apps.get_app_config('lab_9').name, 'lab_9')

    def test_ajax_post_req(self):
        request = {'count' : '1', 'state' : 'False'}
        response = Client().post('/lab_9/', request, content_type='application/json',**{ 'HTTP_X_REQUESTED_WITH' : 'XMLHttpRequest'})
        response = json.loads(response.content)

        new_count = response['count']
        new_state = response['state']

        self.assertEqual(new_count, '2')
        self.assertEqual(new_state, 'True')

        request = {'count' : '4', 'state' : 'True'}
        response = Client().post('/lab_9/', request, content_type='application/json',**{ 'HTTP_X_REQUESTED_WITH' : 'XMLHttpRequest'})
        response = json.loads(response.content)

        new_count = response['count']
        new_state = response['state']

        self.assertEqual(new_count, '3')
        self.assertEqual(new_state, 'False')