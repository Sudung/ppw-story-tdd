from django import forms

class RegistrationForm(forms.Form):
    name = forms.CharField(label = "Name", max_length = 50, widget = forms.TextInput(
        attrs = {
            'class': 'form-attr name-content',
            'oninput': 'checkValid()',
            'placeholder': 'Your Name',
            'required': True,
        }
    ))

    email = forms.EmailField(label = "E-mail", widget = forms.EmailInput(
        attrs = {
            'class': 'form-attr email-content',
            'oninput': 'checkValid()',
            'required': True,
            'placeholder': 'name@email.com',
        }
    ))

    password = forms.CharField(label = "Password", widget = forms.PasswordInput(
        attrs = {
            'class': 'form-attr password-content',
            'oninput': 'checkValid()',
            'required': True,
            'placeholder': 'Password',
        }
    ))