from django.urls import path
from .views import *

app_name = 'lab_10'

urlpatterns = [
    path('', index, name = 'index'),
    path('regist/', regist, name = 'regist'),
    path('validate/', validate, name = 'validate'),
    path('unsub/', unsub, name = 'unsub'),

]