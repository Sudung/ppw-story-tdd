from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from .apps import Lab10Config
from .views import *
from .models import User
from .forms import RegistrationForm
import json

class Lab10UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/lab_10/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab_10/')
        self.assertEqual(found.func, index)
        
    def test_using_add_status_func(self):
        found = resolve('/lab_10/regist/')
        self.assertEqual(found.func, regist)

    def test_using_validate_func(self):
        found = resolve('/lab_10/validate/')
        self.assertEqual(found.func, validate)

    def test_using_index_template(self):
        response = Client().get('/lab_10/')
        self.assertTemplateUsed(response, 'lab_10/index.html')

    def test_model_default_fields(self):
        user = User()
        self.assertEqual(user.name, '')

    def test_add_object_to_models(self):
        User.objects.create()

        total_objects = User.objects.all().count()
        self.assertEqual(total_objects,1)

    def test_landing_request_returns_proper_responses(self):
        response = Client().get('/lab_10/')

        self.assertIn('form', response.context)
        self.assertEqual(response.status_code, 200)

    def test_can_save_regist_POST_request(self):
        request = {'name' : 'NAME', 'email' : 'example@email.com', 'password' : 'PASSWORD'}
        response = Client().post('/lab_10/regist/', request, content_type='application/json', **{ 'HTTP_X_REQUESTED_WITH' : 'XMLHttpRequest'})
        response = response.json()
        self.assertTrue(response['form_is_posted'])

        count_all_available_user = User.objects.all().count()
        self.assertEqual(count_all_available_user, 1)

    def test_form_valid_input(self):
        form = RegistrationForm({'name' : 'NAME', 'email' : 'example@email.com', 'password' : 'PASSWORD'})
        self.assertTrue(form.is_valid())
        
        user = User()
        user.name = form.cleaned_data['name']
        user.save()
        self.assertEqual(str(user), "NAME")

    def test_success_validate_form(self):
        request = {'name' : 'NAME', 'email' : 'example@email.com', 'password' : 'PASSWORD'}
        response = Client().post('/lab_10/validate/', request, content_type='application/json', **{ 'HTTP_X_REQUESTED_WITH' : 'XMLHttpRequest'})
        response = response.json()
        
        self.assertFalse(response['email_is_exist'])
        self.assertTrue(response['form_is_valid'])

    def test_fail_validate_form(self):
        request = {'name' : 'NAME', 'email' : 'email@email.com', 'password' : 'PASSWORD'}
        Client().post('/lab_10/regist/', request, content_type='application/json', **{ 'HTTP_X_REQUESTED_WITH' : 'XMLHttpRequest'})

        request = {'name' : '', 'email' : 'email@email.com', 'password' : 'PASSWORD2'}
        response = Client().post('/lab_10/validate/', request, content_type='application/json', **{ 'HTTP_X_REQUESTED_WITH' : 'XMLHttpRequest'})
        response = response.json()
        
        self.assertTrue(response['email_is_exist'])
        self.assertFalse(response['form_is_valid'])
    
    def test_apps(self):
        self.assertEqual(Lab10Config.name, 'lab_10')
        self.assertEqual(apps.get_app_config('lab_10').name, 'lab_10')
