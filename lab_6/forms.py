from django import forms
from . import models

class StatusForm(forms.Form):
    status = forms.CharField(label = 'Status ', max_length=300, widget=forms.TextInput(attrs = {
        'placeholder' : 'New Status',
        'required' : True,
    }))

