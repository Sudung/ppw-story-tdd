from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from datetime import datetime
from .apps import Lab6Config
from .views import *
from .models import Status
from .forms import StatusForm

class Lab6UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/lab_6/')
        self.assertEqual(response.status_code,200)

    def test_url_not_found(self):
        response = Client().get('/hehe/')
        self.assertEqual(response.status_code,404)

    def test_using_index_func(self):
        found = resolve('/lab_6/')
        self.assertEqual(found.func, index)
        
    def test_using_add_status_func(self):
        found = resolve('/lab_6/add_status/')
        self.assertEqual(found.func, add_status)

    def test_using_index_template(self):
        response = Client().get('/lab_6/')
        self.assertTemplateUsed(response, 'lab_6/index.html')

    def test_model_default_fields(self):
        status = Status()
        self.assertEqual(status.status, '')

    def test_add_object_to_models(self):
        Status.objects.create(status='Sibuk')

        total_objects = Status.objects.all().count()
        self.assertEqual(total_objects,1)

    def test_model_using_correct_datetime(self):
        date = datetime.now()
        Status.objects.create(status='Date Test', date = date)

        status_object = Status.objects.all()
        self.assertTrue(any([str(date) in str(s.date) for s in status_object]))

    def test_landing_contains_content(self):
        response = Client().get('/lab_6/')

        self.assertIn('Hello, Apa kabar?', response.content.decode('utf8'))

    def test_landing_request_returns_proper_responses(self):
        response = Client().get('/lab_6/')

        self.assertIn('form', response.context)
        self.assertIn('statuses', response.context)
        self.assertEqual(response.status_code, 200)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/lab_6/add_status/', {'status' : 'Sibuk'})
        count_all_available_activity = Status.objects.all().count()
        self.assertEqual(count_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/lab_6/')

    def test_form_valid_input(self):
        form = StatusForm({'status': "New Status"})
        self.assertTrue(form.is_valid())
        status = Status()
        status.text = form.cleaned_data['status']
        status.save()
        self.assertEqual(status.text, "New Status")

    def test_form_not_valid_input(self):
        form = StatusForm({'Status': 99})
        self.assertFalse(form.is_valid())

    def test_apps(self):
        self.assertEqual(Lab6Config.name, 'lab_6')
        self.assertEqual(apps.get_app_config('lab_6').name, 'lab_6')

class ChallengeLab6UnitTest(TestCase):
    def test_url_profile_is_exist(self):
        response = Client().get('/lab_6/profile/')
        self.assertEqual(response.status_code,200)

    def test_using_profile_func(self):
        found = resolve('/lab_6/profile/')
        self.assertEqual(found.func, profile)

    def test_using_profile_template(self):
        response = Client().get('/lab_6/profile/')
        self.assertTemplateUsed(response, 'lab_6/profile.html')

    def test_profile_contains_content(self):
        response = Client().get('/lab_6/')
        self.assertIn('Hello, Apa kabar?', response.content.decode('utf8'))