# Generated by Django 2.1.1 on 2018-11-01 06:50

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_6', '0004_auto_20181101_1113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 11, 1, 13, 50, 10, 62102)),
        ),
    ]
